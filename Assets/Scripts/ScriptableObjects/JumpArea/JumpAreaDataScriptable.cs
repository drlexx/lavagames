﻿using UnityEngine;

[CreateAssetMenu(menuName = "Data/Jump", fileName = "JumpData")]
public class JumpAreaDataScriptable : ScriptableObject
{
    public float JumpHeight => _jumpHeight;
    [SerializeField] private float _jumpHeight;

    public float Size => _size;
    [SerializeField] private float _size;
}
