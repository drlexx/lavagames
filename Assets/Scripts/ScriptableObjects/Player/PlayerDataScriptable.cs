﻿using UnityEngine;

[CreateAssetMenu(menuName = "Data/Player", fileName ="PlayerData")]
public class PlayerDataScriptable : ScriptableObject
{
    public float MoveSpeed => _moveSpeed;
    [SerializeField] private float _moveSpeed;

    public float Size => _size;
    [SerializeField] private float _size;
}
