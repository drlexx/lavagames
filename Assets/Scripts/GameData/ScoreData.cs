﻿public class ScoreData : IIntGameData
{
    public string Name => "Score";
    public int Value { get; set; }
}
