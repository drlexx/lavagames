﻿using System.Collections.Generic;
using UnityEngine;

public class InitializationManager : MonoBehaviour
{
    [SerializeField] private PlayerView _playerView;
    [SerializeField] private TapAreaView _tapAreaView;
    [SerializeField] private JumpAreaView _jumpAreaView;
    [SerializeField] private JumpCountView _jumpCountView;
    [SerializeField] private GameAreaView _gameAreaView;

    [SerializeField] private List<JumpAreaDataScriptable> _jumpAreaDatas;
    [SerializeField] private PlayerDataScriptable _playerData;

    private InputController _inputController;
    private PlayerController _playerController;
    private JumpAreaController _jumpAreaController;
    private JumpCounterController _jumpCounterController;
    private GameController _gameController;
    private SaveLoadController _saveLoadController;

    private List<IUpdateble> _updatebleList = new List<IUpdateble>();

    private void Start()
    {
        _inputController = new InputController(_tapAreaView);
        _playerController = new PlayerController(_playerView, _playerData);
        _jumpAreaController = new JumpAreaController(_jumpAreaDatas, _jumpAreaView);
        _saveLoadController = new SaveLoadController();
        _jumpCounterController = new JumpCounterController(_jumpCountView, _saveLoadController, _playerController);
        _gameController = new GameController(_playerController, _jumpAreaController, _gameAreaView, _inputController);

        _updatebleList.Add(_playerController);
        _updatebleList.Add(_gameController);
    }

    private void Update()
    {
        foreach (var item in _updatebleList)
        {
            item.Update();
        }
    }
}

public interface IUpdateble
{
    void Update();
}
