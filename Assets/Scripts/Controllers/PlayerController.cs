﻿using System;
using UnityEngine;

public class PlayerController : IPlayer, IUpdateble, IJumpMaker
{
    public Vector3 Position => _playerView.transform.position;
    public bool IsReadyJump => _behaviour.Current is Run;
    public event Action OnJump;

    private PlayerView _playerView;
    private BehaviourContext _behaviour;
    private PlayerDataScriptable _playerData;

    public PlayerController(PlayerView playerView, PlayerDataScriptable playerData)
    {
        _playerView = playerView;
        _playerData = playerData;
        _behaviour = new BehaviourContext(new Run(playerView.transform, playerData));
    }

    public void JumpTo(Vector3 position, float height)
    {
        _playerView.Jump();
        OnJump?.Invoke();
        _behaviour.Set(new Jump(_playerView.transform, position, height, _playerData, Run));
    }

    private void Run()
    {
        _behaviour.Set(new Run(_playerView.transform, _playerData));
    }

    public void Update()
    {
        _behaviour.Update();
    }

    public void MoveTo(Vector3 position)
    {
        _playerView.transform.position = position;
    }
}

public interface IPlayerBehaviour
{
    void Update();
}