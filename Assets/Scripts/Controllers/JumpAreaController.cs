﻿using System.Collections.Generic;
using UnityEngine;

public class JumpAreaController : IJumper
{
    private JumpAreaView _areaView;

    private List<JumpAreaDataScriptable> _jumpDatas;
    private int _currentDataIndex;

    public JumpAreaController(List<JumpAreaDataScriptable> jumpDatas, JumpAreaView areaView)
    {
        _jumpDatas = jumpDatas;
        _areaView = areaView;
        UpdateSize();
    }

    public void SetNextJumpArea()
    {
        _currentDataIndex = ++_currentDataIndex % _jumpDatas.Count;
        UpdateSize();
    }

    public void UpdateSize()
    {
        _areaView.SetSize(_jumpDatas[_currentDataIndex].Size);
    }

    public float GetHeight()
    {
        return _jumpDatas[_currentDataIndex].JumpHeight;
    }

    public bool IsPositionOnJumper(Vector3 position)
    {
        float xPosition = _areaView.transform.position.x;
        float halfSize = _jumpDatas[_currentDataIndex].Size * .5f;
        return position.x > xPosition - halfSize && position.x < xPosition + halfSize;
    }
}