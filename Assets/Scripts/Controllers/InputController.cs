﻿using System;
using UnityEngine;

public class InputController : IJumpPositionSender
{
    public event Action<Vector3> OnSendPosition;

    public InputController(TapAreaView tapAreaView)
    {
        tapAreaView.OnTapPosition += (x) => OnSendPosition?.Invoke(x);
    }
}
