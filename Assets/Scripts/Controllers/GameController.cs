﻿using System;
using UnityEngine;

public class GameController : IUpdateble, IDisposable
{
    private IPlayer _player;
    private IJumper _jumper;
    private IGameArea _gameArea;
    private IJumpPositionSender _positionSender;

    public GameController(IPlayer player, IJumper jumper, IGameArea gameArea, IJumpPositionSender positionSender)
    {
        _player = player;
        _jumper = jumper;
        _gameArea = gameArea;
        _positionSender = positionSender;

        _positionSender.OnSendPosition += OnFloorTap;
    }

    public void Dispose()
    {
        _positionSender.OnSendPosition -= OnFloorTap;
    }

    private void OnFloorTap(Vector3 position)
    {
        if (_player.IsReadyJump && IsBackClick(position) && _jumper.IsPositionOnJumper(_player.Position))
        {
            _player.JumpTo(new Vector3(position.x, _player.Position.y, _player.Position.z), _jumper.GetHeight());
            _jumper.SetNextJumpArea();
        }
    }

    public void Update()
    {
        if(_player.Position.x > _gameArea.FinishPosition.x)
        {
            _player.MoveTo(_gameArea.StartPosition);
        }
    }

    private bool IsBackClick(Vector3 position)
    {
        return position.x < _player.Position.x;
    }
}

public interface IPlayer
{
    Vector3 Position { get; }
    bool IsReadyJump { get; }
    void JumpTo(Vector3 position, float height);
    void MoveTo(Vector3 position);
}

public interface IJumper
{
    bool IsPositionOnJumper(Vector3 position);
    float GetHeight();
    void SetNextJumpArea();
}

public interface IGameArea
{
    Vector3 StartPosition { get; }
    Vector3 FinishPosition { get;}
}

public interface IJumpPositionSender
{
    event Action<Vector3> OnSendPosition;
}


