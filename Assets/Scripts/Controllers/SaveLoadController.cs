﻿using UnityEngine;

public class SaveLoadController: ISaveLoader
{
    public void Save(IIntGameData gameData)
    {
        PlayerPrefs.SetInt(gameData.Name, gameData.Value);
    }

    public IIntGameData Load(IIntGameData gameData, int defaultValue)
    {
        gameData.Value = PlayerPrefs.GetInt(gameData.Name, defaultValue);
        return gameData;
    }
}

public interface IIntGameData
{
    string Name { get;}
    int Value { get; set; }
}