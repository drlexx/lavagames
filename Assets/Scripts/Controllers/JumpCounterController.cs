﻿using System;

public class JumpCounterController : IDisposable
{
    private IIntGameData _score;
    private ISaveLoader _saveLoader;
    private JumpCountView _jumpCountView;
    private IJumpMaker _jumpMaker;

    public JumpCounterController(JumpCountView jumpCountView, ISaveLoader saveLoader, IJumpMaker jumpMaker)
    {
        _jumpCountView = jumpCountView;
        _score = new ScoreData();
        _saveLoader = saveLoader;
        _jumpMaker = jumpMaker;

        _score = saveLoader.Load(_score, 0);
        jumpCountView.UpdateCount(_score.Value);
        jumpMaker.OnJump += JumpMaker_OnJump;
    }

    public void Dispose()
    {
        _jumpMaker.OnJump -= JumpMaker_OnJump;
    }

    private void JumpMaker_OnJump()
    {
        _score.Value++;
        _jumpCountView.UpdateCount(_score.Value);
        _saveLoader.Save(_score);
    }
}

public interface ISaveLoader
{
    void Save(IIntGameData intGameData);
    IIntGameData Load(IIntGameData intGameData, int defaultValue);
}

public interface IJumpMaker
{
    event Action OnJump;
}