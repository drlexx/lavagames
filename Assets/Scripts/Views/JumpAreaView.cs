﻿using UnityEngine;

public class JumpAreaView : MonoBehaviour
{
    public void SetSize(float size)
    {
        transform.localScale = new Vector3(size, transform.localScale.y, size);
    }
}
