﻿using UnityEngine;

public class PlayerView : MonoBehaviour
{
    [SerializeField] private Animator _animator;

    private const string _jumpTrigger = "IsJump";

    public void Jump()
    {
        _animator.SetTrigger(_jumpTrigger);
    }

    public void MoveTo(Vector3 position)
    {
        transform.position = position;
    }
}
