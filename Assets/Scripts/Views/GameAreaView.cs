﻿using UnityEngine;

public class GameAreaView : MonoBehaviour, IGameArea
{
    public Vector3 StartPosition => _startPosition.position;
    [SerializeField] private Transform _startPosition;
    public Vector3 FinishPosition => _finishPosition.position;
    [SerializeField] private Transform _finishPosition;
}
