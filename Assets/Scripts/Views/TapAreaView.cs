﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class TapAreaView : MonoBehaviour, IPointerClickHandler
{
    public Action<Vector3> OnTapPosition;
    public void OnPointerClick(PointerEventData eventData)
    {
        OnTapPosition?.Invoke(eventData.pointerCurrentRaycast.worldPosition);
    }
}
