﻿using UnityEngine;
using UnityEngine.UI;

public class JumpCountView : MonoBehaviour
{
    [SerializeField] private Text _countText;
    public void UpdateCount(int value)
    {
        _countText.text = value.ToString();
    }
}
