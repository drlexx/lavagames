﻿public class BehaviourContext
{
    public IPlayerBehaviour Current { get; private set; }

    public BehaviourContext(IPlayerBehaviour behaviour)
    {
        Current = behaviour; 
    }

    public void Set(IPlayerBehaviour behaviour)
    {
        Current = behaviour;
    }

    public void Update()
    {
        Current.Update();
    }
}
