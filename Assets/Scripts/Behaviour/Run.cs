﻿using UnityEngine;

public class Run : IPlayerBehaviour
{
    private Transform _transform;
    private PlayerDataScriptable _playerData;

    public Run(Transform transform, PlayerDataScriptable playerData)
    {
        _transform = transform;
        _playerData = playerData;
    }

    public void Update()
    {
        _transform.Translate(Vector3.right * Time.deltaTime * _playerData.MoveSpeed);
    }
}
