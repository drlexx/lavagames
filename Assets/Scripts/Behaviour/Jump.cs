﻿using System;
using UnityEngine;

public class Jump : IPlayerBehaviour
{
    private float _distanceRadius;
    private float _heightRadius;
    private float _trajectoryCenterX;
    private float _trajectoryCenterY;
    private Transform _transform;
    private PlayerDataScriptable _playerData;
    private Action _onEndJump;

    private const float _trajectoryTime = Mathf.PI * .5f;
    private float _currentjumpTime;

    public Jump(Transform playerTransform, Vector3 landPosition, float height, PlayerDataScriptable playerData, Action OnEndJump)
    {
        _transform = playerTransform;
        Vector3 Position = playerTransform.position;
        _playerData = playerData;
        _onEndJump = OnEndJump;

        _distanceRadius = (Position.x - landPosition.x) * .5f;
        _heightRadius = height;
        _trajectoryCenterX = Position.x - _distanceRadius;
        _trajectoryCenterY = Position.y;
        _currentjumpTime = _trajectoryTime;
    }

    public void Update()
    {
        if (_currentjumpTime > -_trajectoryTime)
        {
            float x = _distanceRadius * Mathf.Sin(_currentjumpTime) + _trajectoryCenterX;
            float y = _heightRadius * Mathf.Cos(_currentjumpTime) + _trajectoryCenterY;

            _transform.position = new Vector3(x, y, _transform.position.z);
            _currentjumpTime -= Time.deltaTime * _playerData.MoveSpeed;
        }
        else
        {
            _onEndJump?.Invoke();
        }
    }
}
